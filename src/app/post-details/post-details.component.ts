import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { Posts } from '../posts';
import {switchMap} from 'rxjs/operators';
import { PostsService } from '../posts.service';
import { UsersService } from '../users.service';
import {Comments} from '../comments';
import {Users} from '../users';
import { AuthService } from '../auth.service';



@Component({
  selector: 'app-post-details',
  templateUrl: './post-details.component.html',
  styleUrls: ['./post-details.component.scss']
})


export class PostDetailsComponent implements OnInit {

  posts:Posts[];
  users:Array<Users>=new Array<Users>();
  posts1:Posts=new Posts();
  check:Boolean=true;
  comments:Array<Comments>=new Array<Comments>();
  selectedId:number;
  edit: boolean;
  postBody: string;
  userId: number;
  save: boolean;

  constructor(private router:Router,
    private route:ActivatedRoute,
    private postsService:PostsService,
    private usersService:UsersService,
    private authService: AuthService)
     { }

  ngOnInit() {

     this.route.params.subscribe(params=>
      {
        this.selectedId=params['id'];
      });
  
      this.postsService.getPost(this.selectedId).subscribe(posts=>{this.posts1 = posts; this.postBody = this.posts1.body; this.userId = this.posts1.userId;  this.usersService.getUsers().subscribe(user=>{this.users = user; this.users = this.users.filter(user=>user.id == this.userId)})});

      this.postsService.getComments(this.selectedId).subscribe(comments=>this.comments=comments);
       
      if (localStorage.getItem("user")=== null)
      {
        this.check=false;
      }
  }
  getUserName() {
    return this.authService.getUser();
  }
  addComment(comm:string)
  {
     let comments1 =new Comments;
      comments1.postId=this.selectedId;
      comments1.id=501;
      comments1.body=comm;
      comments1.email="xyz@info.com";
      comments1.name="user1";
      this.comments.push(comments1);
   }

  editPost(post:string) {
    console.log('post-----', post);
    this.edit = true;
    this.posts1.body = post;
    console.log('post1---', this.posts1.body)
  }
  savePost(post:string) {
    console.log('post-----', post);
    this.edit = false;
    this.posts1.body = post;
    console.log('post1---', this.posts1.body);
    this.save = true;
  }
   deleteComment(comm:number) {
    console.log('comm delete', comm)
    console.log('comments---', this.comments);
    this.comments = this.comments.filter(comment => comment.id != comm);
    console.log('comments after filter', this.comments);
  }

}
