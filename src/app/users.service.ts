import { Injectable } from '@angular/core';

import {Observable,Subject, BehaviorSubject} from 'rxjs';
import { HttpHeaders, HttpClient } from '@angular/common/http';

import { Users } from './users';
// import {environment} from '../environments/environment';


const httpOptions= {
 headers:new HttpHeaders({'Content-Type':'application/json'})
};


@Injectable({
  providedIn: 'root'
})
export class UsersService {

   private usersurl='https://jsonplaceholder.typicode.com/users';
 
  private subject=new BehaviorSubject<Users[]>([]);

  public users$=this.subject.asObservable(); 


  constructor(private http:HttpClient) { }

   getUsers():Observable<Users[]>{
     console.log('this.usersurl----', this.usersurl)
    return this.http.get<Users[]>(this.usersurl);
  }

}
