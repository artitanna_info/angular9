import { Component, OnInit } from '@angular/core';
import { PostsListComponent } from '../posts-list/posts-list.component';
import { MatDialogRef, MatInput } from '@angular/material';
import { FormGroupDirective, NgForm, FormBuilder, FormControl, FormGroup, Validators, FormArray, RequiredValidator } from '@angular/forms';
import { Globals } from '../global';
import { Posts } from '../posts';
import { PostsService } from '../posts.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add-post',
  templateUrl: './add-post.component.html',
  styleUrls: ['./add-post.component.scss']
})
export class AddPostComponent implements OnInit {

  postsData: FormGroup;
  i = 1;
  posts: Array<Posts> = new Array<Posts>();
  global: Globals = new Globals();


  constructor(private dialogRef: MatDialogRef<PostsListComponent>,
    private fb: FormBuilder,
    private router: Router,
    private postsService: PostsService) {
    this.postsData = this.fb.group({
      title: ['', Validators.required],
      desc: ['', Validators.required]
    });
  }

  ngOnInit() {
    this.postsService.getPosts().subscribe(posts => this.posts = posts);
  }
  addPost(form) {
    console.log("form---", form)

    debugger
    let posts1 = new Posts;
    posts1.userId = 10 + this.i;
    posts1.id = 100 + this.i;
    this.i++;
    posts1.title = form.title;
    posts1.body = form.desc;
    console.log("this.posts---", this.posts)
    debugger
    console.log("this.posts1---", posts1)
    debugger
    this.posts.push(posts1);
    console.log("this.posts---", this.posts)
    debugger
    this.postsService.setPost(this.posts);
    this.dialogRef.close(this.posts);
  }
  closeDialog(): void {
    this.dialogRef.close();
  }

}
