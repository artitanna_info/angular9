import { Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {PostsService} from '../posts.service';
import {Posts} from '../posts';
import { MatPaginator, MatSort, MatTableDataSource, MatDialogConfig, MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import {AddPostComponent} from '../add-post/add-post.component';
import {Globals} from '../global';
import { EventEmitter } from 'events';
import { Subject, Subscription } from 'rxjs';


@Component({
  selector: 'app-posts-list',
  templateUrl: './posts-list.component.html',
  styleUrls: ['./posts-list.component.scss']
})
export class PostsListComponent implements OnInit {
  
  public p:number =1;
  posts:Array<Posts>=new Array<Posts>();
  check:Boolean=true;

  constructor(private router:Router,
    private postsService:PostsService,
    private dialog:MatDialog,
    private global:Globals ) { }


  ngOnInit() {
    this.postsService.getPosts().subscribe(posts=>this.posts = posts); 
       if (localStorage.getItem("user")=== null) 
        {
           this.check=false;
        }
  }
  
  applyFilter(filterValue:string) 
  {
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase();
    this.postsService.getPosts().subscribe(posts=>this.posts = posts.filter(posts=>posts.title.includes(filterValue)));
  } 

  addPost():void
  {
     const dialogRef=this.dialog.open(AddPostComponent,{
      disableClose: true,
      width: '70%',
      data: null,
      height: '50%'
     });

     dialogRef.afterClosed().subscribe(result=>{
       
        this.postsService.posts$.subscribe(x=>
       {
       this.posts=x;});
      });
  }
  
  viewPost(id:number)
  {
  this.router.navigate(['/post-details',id]);
  }

}
